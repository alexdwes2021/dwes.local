<?php

use DWES\core\App;
use DWES\core\database\Connection;

session_start();

require __DIR__. '/../vendor/autoload.php';

if (isset($_SESSION['idioma']))
    $language = $_SESSION['idioma'];
else
    $language = "es_ES";

putenv("LANGUAGE=$language");
putenv("LC_ALL=$language.utf8");
setlocale(LC_ALL, "$language.utf8");
$var1 = bindtextdomain($language, __DIR__ . "/../locale");
$var2 = bind_textdomain_codeset($language, "UTF-8");
$var3 = textdomain($language);


$config = require __DIR__ . '/../app/config.php';
App::bind('config', $config);

App::bind('connection', Connection:: make ($config['database']));