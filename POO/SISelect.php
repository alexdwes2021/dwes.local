<?php
require_once __DIR__ . '/SelectorIndividual.php';

final class SISelect extends SelectorIndividual
{
    public function generaSelector(): string
    {
        $contador = 0;

        $selector = "<label for='$this->nombre'>$this->titulo</label>";
        $selector .= "<select name='$this->nombre' id='$this->nombre'>";

        foreach ($this->elementos as $texto=>$valor)
        {
            if ($contador++ === $this->indiceSeleccionado)
                $seleccionado = 'selected';
            else
                $seleccionado = '';
            $selector .= "<option value='$valor' $seleccionado>$texto</option>";
        }

        $selector .= "</select>";

        return $selector;
    }
}