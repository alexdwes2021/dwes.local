<?php

$error = '';
$descripcion = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once __DIR__ . '/Imagen.php';

    $imagen = new Imagen($_FILES['imagen']);
    $imagen->uploadImagen();
    $error = $imagen->getLastError();
    if ($error === '')
        $urlFichero = $imagen->getUrlUploadedFile();

    $descripcion = $_POST['descripcion'] ?? '';
}

require __DIR__ . '/views/index.view.php';