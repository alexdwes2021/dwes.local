<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercio POO</title>
</head>
<body>
    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
        <div>
            <?= $radioOpcion->generaSelector() ?>
        </div>
        <div>
            <?= $select->generaSelector() ?>
        </div>
        <input type="submit" value="Enviar">
    </form>
    <?php if($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
        <?= $_POST[$radioOpcion->getNombre()] ?>
        <?= $_POST[$select->getNombre()] ?>
    <?php endif; ?>
</body>
</html>