<?php

interface ISelectorIndividual {
    public function __construct(
        string $titulo,
        string $nombre,
        array $elementos,
        int $indiceSeleccionado
    );

    public function generaSelector() : string;
}