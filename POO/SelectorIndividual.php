<?php
require_once __DIR__ . '/ISelectorIndividual.php';

abstract class SelectorIndividual implements ISelectorIndividual
{
    protected string $titulo;
    protected string $nombre;
    protected array $elementos;
    protected int $indiceSeleccionado;

    public function __construct(
        string $titulo,
        string $nombre,
        array $elementos,
        int $indiceSeleccionado)
    {
        $this->titulo = $titulo;
        $this->nombre = $nombre;
        $this->elementos = $elementos;
        $this->indiceSeleccionado = $indiceSeleccionado;
    }

    abstract public function generaSelector() : string;

    /**
     * @return string
     */
    public function getTitulo(): string
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return SelectorIndividual
     */
    public function setTitulo(string $titulo): SelectorIndividual
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return SelectorIndividual
     */
    public function setNombre(string $nombre): SelectorIndividual
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return array
     */
    public function getElementos(): array
    {
        return $this->elementos;
    }

    /**
     * @param array $elementos
     * @return SelectorIndividual
     */
    public function setElementos(array $elementos): SelectorIndividual
    {
        $this->elementos = $elementos;
        return $this;
    }

    /**
     * @return int
     */
    public function getIndiceSeleccionado(): int
    {
        return $this->indiceSeleccionado;
    }

    /**
     * @param int $indiceSeleccionado
     * @return SelectorIndividual
     */
    public function setIndiceSeleccionado(int $indiceSeleccionado): SelectorIndividual
    {
        $this->indiceSeleccionado = $indiceSeleccionado;
        return $this;
    }


}