<?php
require_once __DIR__ . '/SelectorIndividual.php';

final class SIRadioOpcion extends SelectorIndividual
{
    public function generaSelector(): string
    {
        $contador = 0;

        $selector = "<legend>$this->titulo";

        foreach ($this->elementos as $texto=>$valor)
        {
            if ($contador++ === $this->indiceSeleccionado)
                $seleccionado = 'checked';
            else
                $seleccionado = '';
            $selector .= "<label><input type='radio' name='$this->nombre' value='$valor' $seleccionado>$texto</label>";
        }

        $selector .= "</legend>";

        return $selector;
    }
}