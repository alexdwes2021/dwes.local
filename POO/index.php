<?php
require_once __DIR__ . '/SIRadioOpcion.php';
require_once __DIR__ . '/SISelect.php';

$radioOpcion = new SIRadioOpcion(
    $titulo = 'Selecciona el modo de búsqueda',
    $nombre = 'titulo',
    $elementos = [
        'Título' => 0,
        'Album' => 1,
        'Ambos' => 2
    ],
    $indiceSeleccionado = 0
);

$select = new SISelect(
    $titulo = 'Género musical',
    $nombre = 'genero',
    $elementos = [
        'Todos' => 0,
        'Blues' => 1,
        'Jazz' => 2,
        'Pop' => 3,
        'Rock' => 4
    ],
    $indiceSeleccionado = 2
);

require __DIR__ . '/views/index.view.php';