<?php
require_once __DIR__ . '/Singleton.php';

class Vehiculo
{
    use Singleton;

    private int $numArranques=0;

    public function arrancar()
    {
        $this->numArranques++;
    }

    public function __toString()
    {
        return 'Núm arranques: ' . $this->numArranques;
    }
}