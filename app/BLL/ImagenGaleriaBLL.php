<?php
namespace DWES\app\BLL;

use DWES\app\helpers\UploadFile;

class ImagenGaleriaBLL
{
    use UploadFile;

    private array $allowedFileTypes;
    private string $uploadsDirectory;
    private array $fileProperties;

    public function __construct(array $fileProperties)
    {
        $this->allowedFileTypes = ['image/png', 'image/jpeg'];
        $this->uploadsDirectory = 'images/index/gallery';
        $this->fileProperties = $fileProperties;
    }

    public function uploadImagen()
    {
        $this->uploadFile(
            $this->allowedFileTypes,
            $this->uploadsDirectory,
            $this->fileProperties
        );
    }
}