<?php

use DWES\app\controllers\AuthController;
use DWES\app\controllers\IdiomasController;
use DWES\app\controllers\ImagenGaleriaController;
use DWES\app\controllers\PagesController;
use DWES\core\App;

$router = App::get('router');

$router->get(
    'idioma/:idioma',
    IdiomasController::class,
    'traduce'
);
$router->get(
    'login',
    AuthController::class,
    'login'
);
$router->post('login', AuthController::class, 'checkLogin');
$router->get('logout', AuthController::class, 'logout', 'ROLE_USER');
$router->get(
    '',
    ImagenGaleriaController::class,
    'inicio'
);
$router->get(
    'imagenes',
    ImagenGaleriaController::class,
    'listar',
    'ROLE_USER'
);
$router->get(
    'imagenes/:id',
    ImagenGaleriaController::class,
    'show',
    'ROLE_ADMIN'
);
$router->get(
    'imagenes/:id/delete',
    ImagenGaleriaController::class,
    'delete',
    'ROLE_USER'
);
$router->get('about', 'controllers/about.php', '');
$router->get('blog', PagesController::class, 'blog');
$router->get('single_post', PagesController::class, 'singlePost');
$router->get('contact', 'controllers/contact.php', '');
$router->post(
    'imagenes',
    ImagenGaleriaController::class,
    'nuevaImagen',
    'ROLE_USER'
);
$router->delete(
    'imagenes/:id',
    ImagenGaleriaController::class,
    'deleteJson',
    'ROLE_USER'
);
