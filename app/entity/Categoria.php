<?php
namespace DWES\app\entity;

use DWES\core\database\IEntity;

class Categoria implements IEntity
{
    private int $id;
    private string $nombre;
    private int $numImagenes;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Categoria
     */
    public function setId(int $id): Categoria
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Categoria
     */
    public function setNombre(string $nombre): Categoria
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumImagenes(): int
    {
        return $this->numImagenes;
    }

    /**
     * @param int $numImagenes
     * @return Categoria
     */
    public function setNumImagenes(int $numImagenes): Categoria
    {
        $this->numImagenes = $numImagenes;
        return $this;
    }

    public function toArray() : array
    {
        return [
            'nombre' => $this->nombre,
            'numImagenes' => $this->numImagenes
        ];
    }
}