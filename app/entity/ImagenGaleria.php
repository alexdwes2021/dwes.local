<?php
namespace DWES\app\entity;

use DateTime;
use DWES\core\database\IEntity;

class ImagenGaleria implements IEntity
{
    const RUTA_PORTFOLIO = '../images/index/portfolio/';
    const RUTA_GALLERY = '../images/index/gallery/';
    private int $id;
    private string $nombre;
    private int $categoria;
    private string $descripcion;
    private int $numVisualizaciones;
    private int $numLikes;
    private int $numDownloads;
    private $fechaAlta;
    private int $usuario;

    /**
     * ImagenGaleria constructor.
     */
    public function __construct()
    {
        $this->numVisualizaciones = 0;
        $this->numDownloads = 0;
        $this->numLikes = 0;
        if (is_null($this->fechaAlta))
            $this->fechaAlta = new DateTime();
        else
            $this->fechaAlta = new DateTime($this->fechaAlta);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ImagenGaleria
     */
    public function setId(int $id): ImagenGaleria
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return ImagenGaleria
     */
    public function setNombre(string $nombre): ImagenGaleria
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategoria(): int
    {
        return $this->categoria;
    }

    /**
     * @param int $categoria
     * @return ImagenGaleria
     */
    public function setCategoria(int $categoria): ImagenGaleria
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return ImagenGaleria
     */
    public function setDescripcion(string $descripcion): ImagenGaleria
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumVisualizaciones(): int
    {
        return $this->numVisualizaciones;
    }

    /**
     * @param int $numVisualizaciones
     * @return ImagenGaleria
     */
    public function setNumVisualizaciones(int $numVisualizaciones): ImagenGaleria
    {
        $this->numVisualizaciones = $numVisualizaciones;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumLikes(): int
    {
        return $this->numLikes;
    }

    /**
     * @param int $numLikes
     * @return ImagenGaleria
     */
    public function setNumLikes(int $numLikes): ImagenGaleria
    {
        $this->numLikes = $numLikes;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumDownloads(): int
    {
        return $this->numDownloads;
    }

    /**
     * @param int $numDownloads
     * @return ImagenGaleria
     */
    public function setNumDownloads(int $numDownloads): ImagenGaleria
    {
        $this->numDownloads = $numDownloads;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFechaAlta(): DateTime
    {
        return $this->fechaAlta;
    }

    /**
     * @param DateTime $fechaAlta
     * @return ImagenGaleria
     */
    public function setFechaAlta(DateTime $fechaAlta): ImagenGaleria
    {
        $this->fechaAlta = $fechaAlta;
        return $this;
    }

    public function getPathGaleria()
    {
        return self::RUTA_GALLERY . $this->nombre;
    }

    public function getPathPortfolio()
    {
        return self::RUTA_PORTFOLIO . $this->nombre;
    }

    /**
     * @return int
     */
    public function getUsuario(): int
    {
        return $this->usuario;
    }

    /**
     * @param int $usuario
     * @return ImagenGaleria
     */
    public function setUsuario(int $usuario): ImagenGaleria
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function toArray() : array
    {
        return [
            'nombre' => $this->getNombre(),
            'categoria' => $this->getCategoria(),
            'descripcion' => $this->getDescripcion(),
            'numVisualizaciones' => $this->getNumVisualizaciones(),
            'numLikes' => $this->getNumLikes(),
            'numDownloads' => $this->getNumDownloads(),
            'fechaAlta' => $this->getFechaAlta()->format('Y-m-d H:i:s'),
            'usuario' => $this->getUsuario()
        ];
    }
}