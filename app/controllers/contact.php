<?php
$imagenGaleria = null;
if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    require_once __DIR__ . '/../core/database/Connection.php';
    require_once __DIR__ . '/../entity/ImagenGaleria.php';

    $conexion = Connection::make();

    $sql = 'select * from imagenes where id = :id;';
    $pdoStatement = $conexion->prepare($sql);
    $id = $_POST['id'];
    $pdoStatement->bindParam(':id', $id);
    $result = $pdoStatement->execute();
    $imagenesGaleria = $pdoStatement->fetchAll(PDO::FETCH_CLASS, ImagenGaleria::class);
    $imagenGaleria = $imagenesGaleria[0];
}

require __DIR__ . '/../views/contact.view.php';