<?php

namespace DWES\app\controllers;

use DWES\core\Response;

class PagesController
{
    public function blog()
    {
        Response::renderView('blog');
    }

    public function singlePost()
    {
        Response::renderView('single_post', ['comentarios' => 2]);
    }

    public function notFound()
    {
        header ('HTTP/1.1 404 Not Found', true, 404);
        Response:: renderView ('404');
    }
}