<?php

use DWES\repository\CategoriaRepository;
use DWES\repository\ImagenGaleriaRepository;

$imagenGaleriaRepository = new ImagenGaleriaRepository();
$imagenes = $imagenGaleriaRepository->findAll();

$categoriaRepository = new CategoriaRepository();
$categorias = $categoriaRepository->findAll();

require __DIR__ . '/../views/index.view.php';