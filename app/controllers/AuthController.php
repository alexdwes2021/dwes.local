<?php

namespace DWES\app\controllers;

use DWES\app\helpers\FlashMessage;
use DWES\app\repository\UsuarioRepository;
use DWES\core\App;
use DWES\core\Response;
use DWES\core\Security;

class AuthController
{
    public function login()
    {
        $usuario = App::get('user');
        if (is_null($usuario)) {
            $errorLogin = FlashMessage::get('error-login');
            Response::renderView('login', ['errorLogin' => $errorLogin]);
        }
        else
            App::get('router')->redirect('imagenes');
    }

    public function checkLogin()
    {
        $username = $_POST['username'] ?? '';
        $password = $_POST['password'] ?? '';

        $usuario = App::getRepository(UsuarioRepository::class)->findOneBy([
            'username' => $username
        ]);

        if (Security:: checkPassword (
                $password,
                $usuario->getPassword()
            ) === true)
        {
            $_SESSION['usuario'] = $usuario->getId();
            App::get('router')->redirect('imagenes');
        }

        FlashMessage::set('error-login', "El usuario y/o password introducidos no son correctos");

        App::get('router')->redirect('login');
    }

    public function logout()
    {
        $_SESSION['usuario'] = null;
        unset($_SESSION['usuario']);

        App::get('router')->redirect('login');
    }

    public function unauthorized()
    {
        header ('HTTP/1.1 403 Forbidden', true, 403);
        Response:: renderView ('403');
    }
}