<?php

namespace DWES\app\controllers;

use DWES\core\App;

class IdiomasController
{
    public function traduce(string $idioma)
    {
        $_SESSION['idioma'] = $idioma;

        App::get('router')->redirect('');
    }
}