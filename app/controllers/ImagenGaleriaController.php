<?php
namespace DWES\app\controllers;

use DWES\app\BLL\ImagenGaleriaBLL;
use DWES\core\App;
use DWES\core\Response;
use DWES\app\entity\Categoria;
use DWES\app\entity\ImagenGaleria;
use DWES\app\helpers\MyLogger;
use DWES\app\repository\CategoriaRepository;
use DWES\app\repository\ImagenGaleriaRepository;
use Exception;

class ImagenGaleriaController
{
    public function inicio()
    {
        $imagenes =  App::getRepository(ImagenGaleriaRepository::class)->findAll();
        $categorias = App::getRepository(CategoriaRepository::class)->findAll();

        Response:: renderView ('index', [
            'imagenes' => $imagenes,
            'categorias' => $categorias
        ]);
    }

    public function listar()
    {
        $usuario = App::get('user');
        $categorias = App::getRepository(CategoriaRepository::class)->findAll();
        $imagenes =  App::getRepository(ImagenGaleriaRepository::class)
            ->findBy([
                'usuario' => $usuario->getId()
            ]);

        Response:: renderView ('imagenes', [
            'imagenes' => $imagenes,
            'categorias' => $categorias
        ]);
    }

    public function nuevaImagen()
    {
        $imagenGaleriaRepository = new ImagenGaleriaRepository();

        try {
            $usuario = App::get('user');

            $imagenGaleriaRepository->getConnection()->beginTransaction();

            $categoriaImagen = $_POST['categoria'] ?? 1;
            $descripcion = $_POST['descripcion'];
            $imagenBLL = new ImagenGaleriaBLL($_FILES['imagen']);
            $imagenBLL->uploadImagen();
            $nombre = $imagenBLL->getUploadedFileName();

            $imagen = new ImagenGaleria();
            $imagen->setNombre($nombre)
                ->setCategoria($categoriaImagen)
                ->setDescripcion($descripcion)
                ->setUsuario($usuario->getId());

            $imagenGaleriaRepository->save($imagen);

            $categoriaRepository = new CategoriaRepository();
            /** @var Categoria $categoria */
            $categoria = $categoriaRepository->find($categoriaImagen);
            $categoria->setNumImagenes(
                $categoria->getNumImagenes()+1
            );
            $categoriaRepository->update($categoria);

            $imagenGaleriaRepository->getConnection()->commit();
        } catch(Exception $exception) {
            $imagenGaleriaRepository->getConnection()->rollBack();
            die('No se ha podido insertar la imagen');
        }

        MyLogger::createLog(
            'Se ha insertado una nueva imagen llamada ' . $imagen->getNombre());

        App::get('router')->redirect('imagenes');
    }

    public function show(string $id)
    {
        $imagenGaleriaRepository = new ImagenGaleriaRepository();
        $imagen = $imagenGaleriaRepository->find($id);

        require __DIR__ . '/../views/show-imagen.view.php';
    }

    private function deleteImagen(string $id)
    {
        try {
            $usuario = App::get('user');
            $imagenGaleriaRepository = new ImagenGaleriaRepository();
            $imagenGaleriaRepository->getConnection()->beginTransaction();

            $imagenGaleria = $imagenGaleriaRepository->find($id);
            if ($usuario->getId() !== $imagenGaleria->getUsuario())
                throw new Exception('No puedes borrar imágenes que no hayas creado tú');

            $imagenGaleriaRepository->delete($imagenGaleria);

            $categoriaRepository = new CategoriaRepository();
            /** @var Categoria $categoria */
            $categoria = $categoriaRepository->find($imagenGaleria->getCategoria());
            $categoria->setNumImagenes(
                $categoria->getNumImagenes()-1
            );
            $categoriaRepository->update($categoria);

            $imagenGaleriaRepository->getConnection()->commit();
        } catch(Exception $exception) {
            $imagenGaleriaRepository->getConnection()->rollBack();
            die('No se ha podido eliminar la imagen');
        }
    }

    public function delete(string $id)
    {
        $this->deleteImagen($id);

        App::get('router')->redirect('imagenes');
    }

    public function deleteJson(string $id)
    {
        $this->deleteImagen($id);

        header('Content-Type: application/json');

        echo json_encode([
            'error' => false,
            'mensaje' => "La imagen con id $id se ha eliminado correctamente"
        ]);
    }
}