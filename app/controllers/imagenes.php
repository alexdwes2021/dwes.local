<?php

use DWES\repository\CategoriaRepository;
use DWES\repository\ImagenGaleriaRepository;

$categoriaRepository = new CategoriaRepository();
$categorias = $categoriaRepository->findAll();

$imagenGaleriaRepository = new ImagenGaleriaRepository();
$imagenes = $imagenGaleriaRepository->findAll();

require __DIR__ . '/../views/imagenes.view.php';