    <!-- Principal Content Start -->
   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>IMÁGENES</h1>
       	   <hr>
	       <form class="form-horizontal" action="/imagenes" method="post" enctype="multipart/form-data">
               <legend>Leyenda del formulario</legend>
	       	  <div class="form-group">
	       	  	<div class="col-xs-6">
	       	  		<input class="form-control" type="file" name="imagen">
	       	  	</div>
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">Categoría</label>
	       	  		<select class="form-control" name="categoria">
                        <?php
                        use DWES\app\repository\ImagenGaleriaRepository;
                        use DWES\core\App;

                        foreach ($categorias as $categoria) : ?>
                            <option value="<?= $categoria->getId() ?>"><?= $categoria->getNombre() ?></option>
                        <?php endforeach; ?>
                    </select>
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Descripción</label>
	       	  		<textarea class="form-control" name="descripcion"></textarea>
	       	  		<button type="submit" class="pull-right btn btn-lg sr-button">ENVIAR</button>
	       	  	</div>
	       	  </div>
	       </form>
	       <hr class="divider">
            <div class="table-responsive">
                <table class="table">
                    <caption>Rama con cambios</caption>
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Categoría</th>
                        <th>Fecha alta</th>
                        <th>Operaciones</th>
                    </tr>
                    <?php foreach($imagenes as $imagen) : ?>
                        <tr>
                            <td><img width="100" src="<?= $imagen->getPathGaleria() ?>" alt="<?= $imagen->getDescripcion() ?>"></td>
                            <td><?= $imagen->getNombre() ?></td>
                            <td><?= App::getRepository(ImagenGaleriaRepository::class)->getCategoria($imagen)->getNombre() ?></td>
                            <td><?= $imagen->getFechaAlta()->format('d/m/Y H:i:s') ?></td>
                            <td>
                                <a href="#" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                <a href="/imagenes/<?= $imagen->getId() ?>/delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                <a href="/imagenes/<?= $imagen->getId() ?>" class="btn btn-danger"><i class="fa fa-eye"></i></a>
                                <a class="borrar-json" href="/imagenes/<?= $imagen->getId() ?>" class="btn btn-danger"><i class="fa fa-close"></i></a>
                            </td>
                        </tr>
                    <?php ;endforeach ?>
                </table>
            </div>
           <hr class="divider">
	       <div class="address">
	           <h3>GET IN TOUCH</h3>
	           <hr>
	           <p>Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero.</p>
		       <div class="ending text-center">
			        <ul class="list-inline social-buttons">
			            <li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
			            </li>
			        </ul>
				    <ul class="list-inline contact">
				       <li class="footer-number"><i class="fa fa-phone sr-icons"></i>  (00228)92229954 </li>
				       <li><i class="fa fa-envelope sr-icons"></i>  kouvenceslas93@gmail.com</li>
				    </ul>
				    <p>Photography Fanatic Template &copy; 2017</p>
		       </div>
	       </div>
	    </div>   
   	  </div>
   </div>
<!-- Principal Content Start -->
<script type="text/javascript" src="/js/imagenes.js"></script>
