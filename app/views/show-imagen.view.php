<?php include 'partials/menu.part.php'; ?>

    <!-- Principal Content Start -->
   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>IMÁGEN <?= $imagen->getNombre() ?></h1>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="<?= $imagen->getPathGaleria() ?>" alt="<?= $imagen->getDescripcion() ?>">
                        <div class="caption">
                            <h3>Estadísticas</h3>
                            <ul>
                                <li>Visualizaciones: <?= $imagen->getNumVisualizaciones() ?></li>
                                <li>Likes: <?= $imagen->getNumLikes() ?></li>
                                <li>Descargas: <?= $imagen->getNumDownloads() ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
       	   <hr>
           <hr class="divider">
	       <div class="address">
	           <h3>GET IN TOUCH</h3>
	           <hr>
	           <p>Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero.</p>
		       <div class="ending text-center">
			        <ul class="list-inline social-buttons">
			            <li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
			            </li>
			        </ul>
				    <ul class="list-inline contact">
				       <li class="footer-number"><i class="fa fa-phone sr-icons"></i>  (00228)92229954 </li>
				       <li><i class="fa fa-envelope sr-icons"></i>  kouvenceslas93@gmail.com</li>
				    </ul>
				    <p>Photography Fanatic Template &copy; 2017</p>
		       </div>
	       </div>
	    </div>   
   	  </div>
   </div>
<!-- Principal Content Start -->

<?php include 'partials/footer.part.php'; ?>