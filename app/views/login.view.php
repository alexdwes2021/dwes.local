    <!-- Principal Content Start -->
   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>LOGIN</h1>
       	   <hr>
	       <form class="form-horizontal" action="/login" method="post" enctype="multipart/form-data">
	       	  <div class="form-group">
	       	  	<div class="col-xs-6">
	       	  	    <label for="username" class="label-control">Username</label>
	       	  		<input class="form-control" name="username" id="username">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label for="password" class="label-control">Password</label>
	       	  		<input class="form-control" type="password" name="password" id="password">
	       	  		<button type="submit" class="pull-right btn btn-lg sr-button">ENVIAR</button>
	       	  	</div>
	       	  </div>

               <?php if (!is_null($errorLogin)) : ?>
                   <div class="alert alert-danger">
                       <strong>Error!</strong> <?= $errorLogin ?>
                   </div>
               <?php endif; ?>
	       </form>
	    </div>
   	  </div>
   </div>
