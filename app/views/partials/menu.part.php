<?php use DWES\app\helpers\Utils; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PhotographItem-Responsive Theme</title>

    <!-- Bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <!-- Bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <!-- Magnific-popup css -->
    <link rel="stylesheet" type="text/css" href="../css/magnific-popup.css">
    <!-- Font Awesome icons -->
    <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top">

<!-- Navigation Bar -->
<nav class="navbar navbar-fixed-top navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a  class="navbar-brand page-scroll" href="#page-top">
                <span>[PHOTO]</span>
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="menu">
            <ul class="nav navbar-nav">
                <li class="<?= Utils::isOpcionMenuActiva('index') ? 'active' : '' ?> lien"><a href="/"><i class="fa fa-home sr-icons"></i> Home</a></li>
                <li class="<?= Utils::isOpcionMenuActiva('about') ? 'active' : '' ?> lien"><a href="/about"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
                <li class="<?= Utils::isOpcionMenuActivaInArray(['blog', 'single_post']) ? 'active' : '' ?> lien"><a href="/blog"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
                <li class="<?= Utils::isOpcionMenuActiva('contact') ? 'active' : '' ?> lien"><a href="/contact"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
                <li class="<?= Utils::isOpcionMenuActiva('imagenes') ? 'active' : '' ?> lien"><a href="/imagenes"><i class="fa fa-photo sr-icons"></i> Imágenes</a></li>
                <li class="<?= Utils::isOpcionMenuActiva('login') ? 'active' : '' ?> lien"><a href="/login"><i class="fa fa-user sr-icons"></i> Login</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- End of Navigation Bar -->

