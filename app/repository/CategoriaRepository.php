<?php
namespace DWES\app\repository;

use DWES\core\database\QueryBuilder;
use DWES\app\entity\Categoria;

class CategoriaRepository extends QueryBuilder
{
    /**
     * CategoriaRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'categorias', Categoria::class
        );
    }
}