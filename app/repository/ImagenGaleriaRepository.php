<?php
namespace DWES\app\repository;

use DWES\core\database\QueryBuilder;
use DWES\app\entity\Categoria;
use DWES\app\entity\ImagenGaleria;

class ImagenGaleriaRepository extends QueryBuilder
{
    /**
     * CategoriaRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'imagenes', ImagenGaleria::class
        );
    }

    public function getCategoria(ImagenGaleria $imagenGaleria) : ?Categoria
    {
        $categoriaRepository = new CategoriaRepository();
        return $categoriaRepository->find($imagenGaleria->getCategoria());
    }
}