'use strict';

document.addEventListener('DOMContentLoaded', (event) => {
    let botonesDeleteJson = document.querySelectorAll('.borrar-json');

    botonesDeleteJson.forEach(boton => {
        boton.addEventListener('click', function (event) {
            event.preventDefault();

            fetch(this.href, {method : 'DELETE'})
                .then(respuesta => respuesta.json())
                .then(resp => {
                    this.parentElement.parentElement.remove();
                    alert(resp.mensaje);
                });
        });
    })
});